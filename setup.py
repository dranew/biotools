from distutils.core import setup

setup(name='biotools',
      scripts=['bin/bwaalign'],
      packages=['biotools'],
)
