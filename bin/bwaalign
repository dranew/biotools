#!/usr/bin/env python

import csv
import sys
import logging
import os
import ConfigParser
import itertools
import argparse
import string
import gzip

import pypeliner
import pypeliner.managed as mgd

import biotools.fastqs
import biotools.bams

if __name__ == '__main__':

    argparser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    pypeliner.app.add_arguments(argparser)
    argparser.add_argument('genome', help='Reference genome fasta')
    argparser.add_argument('fastq1', help='Fastq end 1 filename')
    argparser.add_argument('fastq2', help='Fastq end 2 filename')
    argparser.add_argument('output_bam', help='Output bam filename')
    argparser.add_argument('--reads_per_job', type=int, default=1000000, help='Reads per alignment job')
    argparser.add_argument('--config', help='Configuration filename')

    args = vars(argparser.parse_args())

    config = {}

    if args['config'] is not None:
        execfile(args['config'], {}, config)

    config.update(args)

    pyp = pypeliner.app.Pypeline([biotools], config)

    lowmem = {'mem':1}
    medmem = {'mem':8}
    himem = {'mem':32}

    if not os.path.exists(args['genome']+'.bwt'):
        raise Exception('No index for ' + args['genome'])

    pyp.sch.transform('split1', (), lowmem,
        biotools.fastqs.split_fastq,
        None,
        mgd.InputFile(args['fastq1']),
        args['reads_per_job'],
        mgd.TempOutputFile('fastq1', 'byread'))

    pyp.sch.transform('split2', (), lowmem,
        biotools.fastqs.split_fastq,
        None, 
        mgd.InputFile(args['fastq2']), 
        args['reads_per_job'], 
        mgd.TempOutputFile('fastq2', 'byread2'))

    pyp.sch.changeaxis('axis', (), 'fastq2', 'byread2', 'byread')

    pyp.sch.commandline('aln1', ('byread',), medmem,
        'bwa', 'aln',
        args['genome'],
        mgd.TempInputFile('fastq1', 'byread'),
        '>',
        mgd.TempOutputFile('sai1', 'byread'))

    pyp.sch.commandline('aln2', ('byread',), medmem,
        'bwa', 'aln',
        args['genome'],
        mgd.TempInputFile('fastq2', 'byread'),
        '>',
        mgd.TempOutputFile('sai2', 'byread'))

    pyp.sch.commandline('sampe', ('byread',), medmem,
        'bwa', 'sampe',
        args['genome'],
        mgd.TempInputFile('sai1', 'byread'),
        mgd.TempInputFile('sai2', 'byread'),
        mgd.TempInputFile('fastq1', 'byread'),
        mgd.TempInputFile('fastq2', 'byread'),
        '>',
        mgd.TempOutputFile('sam', 'byread'))

    pyp.sch.transform('merge_sams', (), lowmem,
        biotools.bams.merge_sams,
        None,
        mgd.TempInputFile('sam', 'byread'),
        mgd.TempOutputFile('sam'))

    pyp.sch.commandline('bam', (), lowmem,
        'samtools', 'view', '-bt',
        args['genome']+'.fai',
        mgd.TempInputFile('sam'),
        '>',
        mgd.OutputFile(args['output_bam']))

    pyp.run()

