# BIO Tools

A collection of useful bioinformatics tools.

These tools rely on the [pypeliner](https://bitbucket.org/dranew/pypeliner) package for parallelization.

## List of Tools

### bwaalign

Align fastq files using bwa.  Split the fastqs into chunks to allow for alignment in parallel.  Merge
and produce a bam file of the alignments.


