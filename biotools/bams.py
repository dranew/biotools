import shutil


def merge_sams(in_filenames, out_filename):
	""" Merge sam files.

	Args:
		in_filenames (dict): dictionary of input filenames keyed by file identifier
		out_filename (str): output merged sam filename

	"""

	with open(out_filename, 'w') as out_file:

		for in_id, in_filename in in_filenames.iteritems():

			with open(in_filename, 'r') as in_file:

				while True:
					line = in_file.readline()
					if not line.startswith('@'):
						out_file.write(line)
						break

				shutil.copyfileobj(in_file, out_file)


